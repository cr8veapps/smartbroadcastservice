package com.cr8veapps.smartBroadcastService.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

@Entity
@Table(name = "CONTACT")
public class Contacts implements Serializable{
	
	public Contacts() {
		super();
	}
	public Contacts(String name, String mobileNumber, int id) {
		super();
		this.name= name;
		this.mobileNumber=  mobileNumber;
		this.id = id;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -5072210482887213404L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	private int id;
	
	@Column(name = "NAME")
	private String name;
	
	@Column(name = "DISPLAY_NAME")
	private String displayName;
	
	@Column(name = "MOBILE_NUMBER")
	private String mobileNumber;
	
	@Column(name = "DISTRICT_ID")
	private int districtId;
	
	@Column(name = "PANCHAYAT_ID")
	private int panchayatId;
	
	@Column(name = "TALUK_ID")
	private int talukId;
	
	@Column(name = "USER_ID")
	private int userId;
	
    @OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    @JoinColumn(name = "CONTACT_ID")
    private Set<ContactCategory> contactCatagory = new HashSet<>();
    
	
	@Column(name = "CREATED_BY")
	private String createdBy;
	
	@Column(name = "CREATED_TS")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdTs;
	
	@Column(name = "UPDATED_BY")
	private String updatedBy;
	
	@Column(name = "UPDATED_TS")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedTs;
	
	@OneToOne(cascade = CascadeType.ALL )
    @JoinColumn(name = "DISTRICT_ID", referencedColumnName = "ID" , insertable=false, updatable=false)
	@NotFound(action = NotFoundAction.IGNORE)
	private District district;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public int getDistrictId() {
		return districtId;
	}

	public void setDistrictId(int districtId) {
		this.districtId = districtId;
	}

	public int getPanchayatId() {
		return panchayatId;
	}

	public void setPanchayatId(int panchayatId) {
		this.panchayatId = panchayatId;
	}

	public int getTalukId() {
		return talukId;
	}

	public void setTalukId(int talukId) {
		this.talukId = talukId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public Set<ContactCategory> getContactCatagory() {
		return contactCatagory;
	}

	public void setContactCatagory(Set<ContactCategory> contactCatagory) {
		this.contactCatagory = contactCatagory;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedTs() {
		return createdTs;
	}

	public void setCreatedTs(Date createdTs) {
		this.createdTs = createdTs;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedTs() {
		return updatedTs;
	}

	public void setUpdatedTs(Date updatedTs) {
		this.updatedTs = updatedTs;
	}
 
	public District getDistrict() {
		return district;
	}

	public void setDistrict(District district) {
		this.district = district;
	}

	
}
