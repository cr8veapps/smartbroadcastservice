/**
 * 
 */
package com.cr8veapps.smartBroadcastService.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cr8veapps.smartBroadcastService.model.State;

/**
 * @author anush
 *
 */
@Repository
public interface StateRepository extends JpaRepository<State, Long> {

}
