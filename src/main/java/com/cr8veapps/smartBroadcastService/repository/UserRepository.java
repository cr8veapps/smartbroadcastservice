/**
 * 
 */
package com.cr8veapps.smartBroadcastService.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cr8veapps.smartBroadcastService.model.User;

/**
 * @author anush
 *
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long>{

	User getUserByEmail(String email);

}
