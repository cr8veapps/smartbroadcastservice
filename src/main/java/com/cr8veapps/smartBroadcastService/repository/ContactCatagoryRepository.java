/**
 * 
 */
package com.cr8veapps.smartBroadcastService.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.cr8veapps.smartBroadcastService.model.ContactCategory;

/**
 * @author anush
 *
 */
@Repository
public interface ContactCatagoryRepository extends JpaRepository<ContactCategory, Long>{
	
	@Query("SELECT p FROM ContactCategory p WHERE p.contactId = :contactId")
	List<ContactCategory> findBycontact(int contactId);

}
