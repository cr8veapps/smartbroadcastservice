/**
 * 
 */
package com.cr8veapps.smartBroadcastService.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cr8veapps.smartBroadcastService.model.UserDetails;

/**
 * @author anush
 *
 */
public interface UserDetailsRepository extends JpaRepository<UserDetails, Long>{

}
