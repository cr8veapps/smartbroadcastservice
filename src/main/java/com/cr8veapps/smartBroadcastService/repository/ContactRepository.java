package com.cr8veapps.smartBroadcastService.repository;

import java.util.List;

import javax.validation.Valid;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.cr8veapps.smartBroadcastService.model.Contacts;

@Repository
public interface ContactRepository extends JpaRepository<Contacts, Long> {

	@Query("SELECT p FROM Contacts p WHERE p.userId = :userId ORDER BY p.name")
	List<Contacts> findByUserId(@Valid int userId);

	@Query("SELECT p FROM Contacts p WHERE p.id = :id")
	Contacts findContactsById(int id);

	@Query("SELECT p FROM Contacts p WHERE p.name = :name and p.mobileNumber = :mobile")
	Contacts findByMobileAndName(String name, String mobile);

	@Query("SELECT new com.cr8veapps.smartBroadcastService.model.Contacts(p.name,  p.mobileNumber ,p.id) FROM Contacts p WHERE p.userId = :userId and "
			+ "(:districtId = 0 or p.districtId = :districtId) and "
			+ "(:talukId = 0 or p.talukId= :talukId) and"
			+ "(:panchayatId = 0 or  p.panchayatId = :panchayatId )")
	List<Contacts> findByFilters(@Valid int userId, int districtId, int talukId, int panchayatId);

}
