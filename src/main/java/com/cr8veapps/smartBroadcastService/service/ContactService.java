/**
 * 
 */
package com.cr8veapps.smartBroadcastService.service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cr8veapps.smartBroadcastService.dto.ContactFilter;
import com.cr8veapps.smartBroadcastService.dto.ResponseEntity;
import com.cr8veapps.smartBroadcastService.model.ContactCategory;
import com.cr8veapps.smartBroadcastService.model.Contacts;
import com.cr8veapps.smartBroadcastService.repository.ContactCatagoryRepository;
import com.cr8veapps.smartBroadcastService.repository.ContactRepository;

/**
 * @author anush
 *
 */
@Service
public class ContactService {
	
	@Autowired
	ContactRepository customerRepository;
	
	@Autowired
	ContactCatagoryRepository contactCatagoryRepository;

	@Transactional
	public ResponseEntity<List<Contacts>> getContactsByUserId(@Valid int userId) {
		ResponseEntity<List<Contacts>> response = new ResponseEntity<>();
		try {
			List<Contacts> res = customerRepository.findByUserId(userId);
			response.setStatusCode(200);
			response.setResponse(res);
		} catch (Exception e) {
			e.printStackTrace();
			response.setStatusCode(500);
			response.setErrorResponse("Error in getting picklist");
		}
		return response;
	}

	@Transactional
	public ResponseEntity<Contacts> createOrUpdateContact(@Valid Contacts contact) {
		ResponseEntity<Contacts> response = new ResponseEntity<>();
		try {
			Set<ContactCategory> ccList = new HashSet<>();
			ccList =  contact.getContactCatagory();
			
			Contacts contacts = new Contacts();
			if(0 == contact.getId()) {
				contact.setContactCatagory(null);
				contacts =customerRepository.findByMobileAndName(contact.getName(), contact.getMobileNumber());
				if(null != contacts) {
					contact.setId(contacts.getId());
				}
			}
			 contacts =  customerRepository.save(contact);
			for(ContactCategory cc :ccList ) {
				if(cc.getId() ==0) {
					cc.setContactId(contacts.getId());
				}
				contactCatagoryRepository.save(cc);
			}
			contacts = customerRepository.findContactsById(contacts.getId());
			response.setResponse(contacts);
			response.setStatusCode(200);
		} catch (Exception e) {												
			// TODO Auto-generated catch block
			e.printStackTrace();
			response.setErrorResponse("Error in saving contacts");
			response.setStatusCode(500);
		}
		return response;
	}

	public ResponseEntity<List<Contacts>> getContactsByFilters(@Valid int userId, @Valid ContactFilter contactFilter) {
		ResponseEntity<List<Contacts>> response = new ResponseEntity<>();
		try {
			List<Contacts> res = customerRepository.findByFilters(userId ,contactFilter.getDistrictId(),contactFilter.getTalukId(),contactFilter.getPanchayatId());
			response.setStatusCode(200);
			response.setResponse(res);
		} catch (Exception e) {
			e.printStackTrace();
			response.setStatusCode(500);
			response.setErrorResponse("Error in getting picklist");
		}
		return response;
	}

}
