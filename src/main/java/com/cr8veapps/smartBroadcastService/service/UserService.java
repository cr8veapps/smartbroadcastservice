/**
 * 
 */
package com.cr8veapps.smartBroadcastService.service;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cr8veapps.smartBroadcastService.dto.ResponseEntity;
import com.cr8veapps.smartBroadcastService.model.User;
import com.cr8veapps.smartBroadcastService.model.UserDetails;
import com.cr8veapps.smartBroadcastService.repository.UserDetailsRepository;
import com.cr8veapps.smartBroadcastService.repository.UserRepository;

/**
 * @author anush
 *
 */
@Service
public class UserService {
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	UserDetailsRepository userDetailsRepository;
	
	@Transactional
	public ResponseEntity<User> createOrUpdateUser(@Valid User user) {
		ResponseEntity<User> response = new ResponseEntity<>();
		try {
			User userRec =  userRepository.getUserByEmail(user.getUserDetails().getEmail());
			if(null== userRec) {
				UserDetails userDetails = new UserDetails();
				userDetails = user.getUserDetails();
				user.setUserDetails(null);
				userRec= userRepository.save(user);
				userDetails.setUser(userRec);
				userDetails= userDetailsRepository.save(userDetails);
				userRec.setUserDetails(userDetails);
				response.setResponse(userRec);
				response.setStatusCode(200);
			}else {
				response.setResponse(userRec);
				response.setStatusCode(200);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			response.setErrorResponse("Error in saving user !! please try again ");
			response.setStatusCode(500);
		}
		return response;
	}

}
