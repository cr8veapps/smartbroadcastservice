/**
 * 
 */
package com.cr8veapps.smartBroadcastService.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cr8veapps.smartBroadcastService.dto.ResponseEntity;
import com.cr8veapps.smartBroadcastService.model.State;
import com.cr8veapps.smartBroadcastService.repository.StateRepository;

/**
 * @author anush
 *
 */
@Service
public class CommonsService {
	
	@Autowired
	StateRepository stateRepository;
	
	@Transactional
	public ResponseEntity<List<State>> getAllState() {
		ResponseEntity<List<State>> response = new ResponseEntity<>();
		try {
			List<State> res = stateRepository.findAll();
			response.setStatusCode(200);
			response.setResponse(res);
		} catch (Exception e) {
			e.printStackTrace();
			response.setStatusCode(500);
			response.setErrorResponse("Error in getting picklist");
		}
		return response;	
	}
}
