/**
 * 
 */
package com.cr8veapps.smartBroadcastService.dto;

/**
 * @author anush
 *
 */
public class FilteredContacts {
	private String name;
	private String mobileNumber;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	

}
