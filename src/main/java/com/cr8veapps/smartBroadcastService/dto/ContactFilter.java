/**
 * 
 */
package com.cr8veapps.smartBroadcastService.dto;

/**
 * @author anush
 *
 */
public class ContactFilter {

	private int districtId;
	
	private int panchayatId;
	
	private int talukId;
	
	private int userId;

	public int getDistrictId() {
		return districtId;
	}

	public void setDistrictId(int districtId) {
		this.districtId = districtId;
	}

	public int getPanchayatId() {
		return panchayatId;
	}

	public void setPanchayatId(int panchayatId) {
		this.panchayatId = panchayatId;
	}

	public int getTalukId() {
		return talukId;
	}

	public void setTalukId(int talukId) {
		this.talukId = talukId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}
	
	
	

}
