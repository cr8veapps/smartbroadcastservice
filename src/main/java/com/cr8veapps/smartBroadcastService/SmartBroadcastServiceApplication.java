package com.cr8veapps.smartBroadcastService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories("com.cr8veapps.smartBroadcastService.repository")
public class SmartBroadcastServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SmartBroadcastServiceApplication.class, args);
	}

}
