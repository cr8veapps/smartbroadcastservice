/**
 * 
 */
package com.cr8veapps.smartBroadcastService.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cr8veapps.smartBroadcastService.dto.ContactFilter;
import com.cr8veapps.smartBroadcastService.dto.ResponseEntity;
import com.cr8veapps.smartBroadcastService.model.Contacts;
import com.cr8veapps.smartBroadcastService.model.State;
import com.cr8veapps.smartBroadcastService.model.User;
import com.cr8veapps.smartBroadcastService.service.CommonsService;
import com.cr8veapps.smartBroadcastService.service.ContactService;
import com.cr8veapps.smartBroadcastService.service.UserService;

/**
 * @author anush
 *
 */
@RestController
@RequestMapping("/smartbroadcast")
public class SmartBroadcastController {
	
	@Autowired
	CommonsService commonsService;
	
	@Autowired
	ContactService contactService;
	
	@Autowired
	UserService userService;
	
	@PostMapping("/addUser")
	  public  ResponseEntity<User> createUpdateUser(@Valid @RequestBody User user) {
	    return userService.createOrUpdateUser(user);
	  }
	 
	 @PostMapping("/addContact")
	  public ResponseEntity<Contacts> addContact(@Valid @RequestBody Contacts contact) {
	    return contactService.createOrUpdateContact(contact);
	  }
	 
	 @GetMapping("/getContact")
	 public ResponseEntity<List<Contacts>> getContact(@Valid @RequestParam int userId) {
		 return contactService.getContactsByUserId(userId);
	 }
	 
	 @GetMapping("/getContactByFilter")
	 public ResponseEntity<List<Contacts>> getContactByFilter(@Valid @RequestParam int userId ,
			 @Valid @RequestParam int districtId,
			 @Valid @RequestParam int panchayatId,
			 @Valid @RequestParam int talukId) {
		 ContactFilter contactFilter = new ContactFilter();
		 contactFilter.setDistrictId(districtId);
		 contactFilter.setPanchayatId(panchayatId);
		 contactFilter.setTalukId(talukId);
		 contactFilter.setUserId(userId);
		 return contactService.getContactsByFilters(userId ,contactFilter);
	 }
	 
	 @GetMapping("/getpicklist")
	 public ResponseEntity<List<State>> getStateList() {
		 	return commonsService.getAllState();
		 	
	 }
	 
}
